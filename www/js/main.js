document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    navigator.splashscreen.hide();
    var acelerar = new Acelerar();

    $('#startButton').on('click', function () {
        acelerar.init();
    });

    $('#stopButton').on('click', function () {
        acelerar.stop();
    });

    $('#cleanButton').on('click', function () {
        acelerar.clean();
    });
}

function Acelerar() {
    this.watchId = 0;
}

Acelerar.prototype = {
    init: function () {
        options = {frequency: 1000};
        this.watchId = navigator.accelerometer.watchAcceleration(this.onSucces, this.onError, options);
    },
    stop: function () {
        navigator.accelerometer.clearWatch(this.watchId);
    },
    clean: function () {
        $('#spanDirectionX').html('');
        $('#spanDirectionY').html('');
        $('#spanDirectionZ').html('');
        $('#spanTimeStamp').html('');
    },
    onSucces: function (acceleration) {
        $('#spanDirectionX').html(acceleration.x.toFixed(2));
        $('#spanDirectionY').html(acceleration.y.toFixed(2));
        $('#spanDirectionZ').html(acceleration.z.toFixed(2));
        $('#spanTimeStamp').html(acceleration.timestamp);
    },
    onError: function () {
    }
};